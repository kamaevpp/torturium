#!/usr/bin/env python3
import argparse
import logging

from torturium import main


def parse_loglevel(level):
    real_level = getattr(logging, level.upper())
    if isinstance(real_level, int):
        return real_level
    raise RuntimeError('Incorrect loglevel: {}'.format(level))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--loglevel', '-l', default='INFO')
    parser.add_argument('--host', default='0.0.0.0')
    parser.add_argument('--port', '-p', default='8888')

    args = parser.parse_args()

    logging.basicConfig(level=parse_loglevel(args.loglevel))
    main(args.host, args.port)
