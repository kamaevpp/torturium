from tornado.web import RequestHandler


class WebUI(RequestHandler):
    def get(self):
        self.render('index.html')
