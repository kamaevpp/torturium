import re

COMMANDS = {}
NAME_REGEXP = re.compile(r'\w{3,20}', re.U)

# factory of messages
def parse(text):
    if ' ' in text:
        command, body = text.split(' ', 1)
    else:
        command, body = text, ''
    return COMMANDS.get(command.lower(), COMMANDS['unknown'])(body)


class RegisteredCommand(type):
    def __init__(cls, name, bases, attrs):
        COMMANDS[name.lower()] = cls
        super().__init__(name, bases, attrs)


class Command(metaclass=RegisteredCommand):
    def __init__(self, text):
        self.errors = []
        self.__body = text
        self.is_validated = False

    def validate(self):
        self.is_validated = True

    def is_valid(self):
        if not self.is_validated:
            self.validate()
        return not self.errors


class Unknown(Command):
    def validate(self):
        self.errors.append('Command is unknown')


class Login(Command):
    NICK_REGEXP = NAME_REGEXP

    def __init__(self, text):
        super(Login, self).__init__(text)
        self.nick = text

    def validate(self):
        if not self.NICK_REGEXP.match(self.nick):
            self.errors.append('Incorrect nick: "{}"'.format(self.nick))


class Room:
    ROOM_REGEXP = NAME_REGEXP

    def __init__(self, text):
        super().__init__(text)
        self.room = text

    def validate(self):
        if not self.ROOM_REGEXP.match(self.room):
            self.errors.append('Incorrect room name: "{}"'.format(self.room))


class Leave(Room, Command):
    pass
class Join(Room, Command):
    pass


class Write(Command):
    def __init__(self, text):
        super().__init__(text)
        if ' ' in text:
            self.room, self.body = text.split(' ', 1)
        else:
            self.errors.append('Empty message body')


class Exit(Command):
    def validate(self):
        if self.__body:
            self.errors.append('Body should be empty')
        super().validate()
